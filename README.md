# grunt-validate-json

> Validate your JSON files via jsonschema

## Not stable
This grunt plugin is still under development.

Although it provides basic functionality, it has not been tested well. __Use in production is deprecated__.

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-validate-json --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-validate-json');
```

## The "validate_json" task

### Overview
In your project's Gruntfile, add a section named `validate_json` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
	validate_json: {
		options: {
			fatal: true
		},
		files: [{
			src: [projectConfigPath + '*.json'],
			dest: schemesPath,
			ext: '.schema.json'
		}]
	}
});
```

Pass in the files to validate as source files and your schemes as their destination.

### Options

#### options.fatal
Type: `Boolean`
Default value: `false`

Set if the task should fail on a validation error.


## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
_(Nothing yet)_
