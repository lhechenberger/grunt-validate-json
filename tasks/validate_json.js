/*
 * grunt-validate-json
 * https://github.com/lukas/grunt-validate-json
 *
 * Copyright (c) 2015 Lukas Hechenberger
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask('validate_json', 'Validate your JSON files via jsonschema', function() {
    // Merge task-specific and/or target-specific options with these defaults.
    var options = this.options({
      fatal: false
    });
    
    var validator = new (require('jsonschema').Validator);
    
    var errorsCount = 0;
    var validationMessage = function(validation, src) {
	    if (validation.errors.length > 0) {
		    errorsCount += validation.errors.length
		    grunt.verbose.error();
		    
		    for (var i = 0; i < validation.errors.length; i++) {
			    grunt.log.error('Error in "' + src.path + '": ' + validation.errors[i]);
			    if (validation.errors[i].schema.description) {
				    grunt.log.writeln(("   Instance description: " + validation.errors[i].schema.description).grey);
				}
				if (validation.errors[i].schema.type) {
				    grunt.log.writeln(("   Instance type: " + validation.errors[i].schema.type).grey);
			    }
		    }
	    }
	    else {
		    grunt.verbose.ok();
	    }
    }

    // Iterate over all specified file groups.
    this.files.forEach(function(f) {
      // Concat specified files.
      var src = f.src.filter(function(filepath) {
        // Warn on and remove invalid source files (if nonull was set).
        if (!grunt.file.exists(filepath)) {
          grunt.log.warn('Source file "' + filepath + '" not found.');
          return false;
        } else {
          return true;
        }
      }).map(function(filepath) {
        // Read file source.
        return { data: grunt.file.readJSON(filepath), path: filepath };
      });

      // Handle options.
      //src.content += options.punctuation;
	  
	  if (!f.dest && !options.schema) {
		  grunt.fail.warn('Could not get scheme: Pass it (or its dir) as destination or via options.schema');
	  }
	  
	  var schema = false;
	  var searchSchema = true;
	  
	  if (options.schema && !grunt.file.isDir(options.schema)) {
		  searchSchema = false
	      schema = grunt.file.readJSON(options.schema);
	  }
	  else if (f.dest && !grunt.file.isDir(f.dest)) {
		  searchSchema = false
	      schema = grunt.file.readJSON(f.dest);
	  }
	  
	  for (var i = 0; i < src.length; i++) {
	    if (searchSchema) { // try to get scheme from src name
	    	grunt.verbose.write("Searching for schema at " + f.dest + "...");
	    	
	    	var components = src[i].path.split("/")
	    	var name = components[components.length - 1];
	    	
	    	if (f.ext) {
		    	var parts = name.split(".")
		    	name = parts[parts.length - 2] + f.ext;
	    	}
	    	
	    	var schemaPath = f.dest + name;
	    	if (grunt.file.exists(schemaPath)) {
		    	grunt.verbose.ok();
		    	schema = grunt.file.readJSON(schemaPath);
	    	}
	    	else {
		    	grunt.verbose.error();
		    	grunt.fail.warn("Schema file does not exist at " + schemaPath);
	    	}
	    }
	    grunt.verbose.write('Validating ' + src[i].path + ' agains schema ' + (schema.title || f.dest) + '...');
	    
	    validationMessage(validator.validate(src[i].data, schema), src[i]);
	  }
    });
    
    if (errorsCount > 0) {
	    grunt.fail[options.fatal ? "fatal" : "warn"](errorsCount + ' error(s) occured.');
    }
    else {
	    grunt.log.ok();
    }
  });

};
